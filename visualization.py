import matplotlib.pyplot as pp
from random_walk import RandomWalk

if __name__ == '__main__':
    while True:
        rw = RandomWalk(60_000)
        rw.fill_walk()

        # Visualize data
        fig, ax = pp.subplots()

        ax.set_title("Random Walk", fontsize=24)

        # Colormap to show the order of the points in the walk
        point_numbers = range(rw.num_points)
        ax.scatter(rw.x_values, rw.y_values, s=2, c=point_numbers, cmap=pp.cm.Blues, edgecolors='none')

        ax.set_aspect('equal')

        # Remove the axes.
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # Emphasize the first and last points.
        ax.scatter(0, 0, c='lightgreen', edgecolors='none', s=90)
        ax.scatter(rw.x_values[-1], rw.y_values[-1], c='lightpink', edgecolors='none', s=90)

        pp.savefig('random_walk.png', bbox_inches='tight')
        pp.show()

        keep_running = input("Make another walk? (y/n): ")
        if keep_running in ('n', 'N', 'no', 'No', 'NO'):
            break
