# Random Walk Visualizer
![random_walk.png](random_walk.png)

# Setup (linux)
- Inside the root directory (`random_walk`), install `requirements.txt`:
    ```shell
    python -m venv venv && \  
    source venv/bin/activate && \
    pip install -r requirements.txt
    ```
- Run the program
    ```shell
    python visualization.py
    ```
