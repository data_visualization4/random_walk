from random import choice


class RandomWalk:
    """A class to generate random walks."""

    MIN_DISTANCE = 0
    MAX_DISTANCE = 6
    DIRECTIONS = [1, -1]

    def __init__(self, num_points=5000):
        self.num_points = num_points

        # Starts at (0, 0)
        self.x_values = [0]
        self.y_values = [0]

    def fill_walk(self) -> None:
        """Calculate all the points in the walk."""
        while len(self.x_values) < self.num_points:
            # Decide which direction to go, and how far to go.
            x_step = self.get_step()
            y_step = self.get_step()

            # Reject moves that go nowhere.
            if x_step == 0 and y_step == 0:
                continue

            # Add the new position.
            self.x_values.append(self.x_values[-1] + x_step)
            self.y_values.append(self.y_values[-1] + y_step)

    def get_step(self) -> int:
        direction = choice(RandomWalk.DIRECTIONS)
        distance = choice(range(RandomWalk.MIN_DISTANCE, RandomWalk.MAX_DISTANCE))
        return direction * distance
